﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
   public class Participantes
    {
        private int _idParticipante;
        private int _numeroAsociado;
        private string _nombre;
        private string _apellidoP;
        private string _apellidoM;
        private string _direccion;
        private string _telefono;
        private string _pais;
        private string _foto;

        public int IdParticipante { get => _idParticipante; set => _idParticipante = value; }
        public int NumeroAsociado { get => _numeroAsociado; set => _numeroAsociado = value; }
        public string Nombre { get => _nombre; set => _nombre = value; }
        public string ApellidoP { get => _apellidoP; set => _apellidoP = value; }
        public string ApellidoM { get => _apellidoM; set => _apellidoM = value; }
        public string Direccion { get => _direccion; set => _direccion = value; }
        public string Telefono { get => _telefono; set => _telefono = value; }
        public string Pais { get => _pais; set => _pais = value; }
        public string Foto { get => _foto; set => _foto = value; }
    }
}
