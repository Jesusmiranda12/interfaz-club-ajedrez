﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Pais
    {
        private int _idPaid;
        private string _nombrePais;
        private int _club;
        private string _paisqueRepresenta;

        public int IdPaid { get => _idPaid; set => _idPaid = value; }
        public string NombrePais { get => _nombrePais; set => _nombrePais = value; }
        public int Club { get => _club; set => _club = value; }
        public string PaisqueRepresenta { get => _paisqueRepresenta; set => _paisqueRepresenta = value; }
    }
}
