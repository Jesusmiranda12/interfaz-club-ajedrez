﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Entidades;
using System.Data;

namespace AccesoaDatos
{
   public class ParticipantesAccesoDatos
    {
        ConexionAccesoDatos conexion = new ConexionAccesoDatos("localhost", "root", "", "clubajedrez", 3306);


        public void Guardar(Participantes participante)
        {
            if (participante.IdParticipante == 0)
            {
                //codigo para insertar
                string consulta = string.Format("call  p_insertarParticipantes(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')",
                participante.NumeroAsociado,participante.Nombre,participante.ApellidoP,participante.ApellidoM,
                participante.Direccion,participante.Telefono,participante.Pais,participante.Foto);
                conexion.Ejecutarconsulta(consulta);
            }
            else
            {
                //update o que lo modifique
                string consulta = string.Format("call p_actualizarParticipante('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')", participante.NumeroAsociado, participante.Nombre, participante.ApellidoP, participante.ApellidoM, participante.Direccion, participante.Telefono, participante.Pais,participante.Foto,participante.IdParticipante);
                conexion.Ejecutarconsulta(consulta);
            }
        }

        public void Eliminar(int id)
        {
            //eliminar
            string consulta = string.Format("p_eliminarParticipante('{0}')", id);
            conexion.Ejecutarconsulta(consulta);
        }
        public List<Participantes> GetParticipantes(string filtro)
        {
            //List<Usuario> listusuario = new List<Usuario>();
            var listParticipantes = new List<Participantes>(); //la variable var es generica
            var ds = new DataSet();
            string consulta = "call p_verParticipantesconpais('"+filtro+"')";
         //   string consulta = "Select * from v_alumnos where Nombrealumno like '%" + filtro + "%'";
            ds = conexion.Obtenerdatos(consulta, "p_verParticipantesconpais");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var participante = new Participantes
                {
                    IdParticipante=Convert.ToInt32(row["idparticipante"]),
                    NumeroAsociado=Convert.ToInt32(row["numero_asociado"]),
                
                    Nombre = row["nombre"].ToString(),
                    ApellidoP=row["apellidop"].ToString(),
                    ApellidoM = row["apellidom"].ToString(),
                    Direccion=row["direccion"].ToString(),
                    Telefono=row["telefono"].ToString(),
                    Pais=row["paise que representa"].ToString(),
                    Foto=row["foto"].ToString()
                };
                listParticipantes.Add(participante);

            }
            //HardCodear
            //lenar lista
            return listParticipantes;
        }

    }
}
