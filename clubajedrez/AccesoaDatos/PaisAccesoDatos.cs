﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using System.Data;

namespace AccesoaDatos
{
    public class PaisAccesoDatos
    {
        ConexionAccesoDatos conexion = new ConexionAccesoDatos("localhost", "root", "", "clubajedrez", 3306);
        public List<Pais> GetPaises(string filtro)
        {
            //List<Usuario> listusuario = new List<Usuario>();
            var listPais = new List<Pais>(); //la variable var es generica
            var ds = new DataSet();
            string consulta = "call p_verPaises('" + filtro + "')";
            //   string consulta = "Select * from v_alumnos where Nombrealumno like '%" + filtro + "%'";
            ds = conexion.Obtenerdatos(consulta, "p_verPaises");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var pais = new Pais
                {
                    IdPaid=Convert.ToInt32(row["idpais"]),
                    NombrePais=row["nombre"].ToString(),
                   // Club=Convert.ToInt32(row["numeroclubs"]),
                    //PaisqueRepresenta=row["fkpaisrepresentante"].ToString()


                };
                listPais.Add(pais);

            }
            //HardCodear
            //lenar lista
            return listPais;
        }
    }
}
