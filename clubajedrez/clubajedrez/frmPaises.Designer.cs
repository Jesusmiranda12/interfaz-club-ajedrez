﻿namespace clubajedrez
{
    partial class frmPaises
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPaises));
            this.btnregresar = new System.Windows.Forms.Button();
            this.gpbPaises = new System.Windows.Forms.GroupBox();
            this.cmbPAIS = new System.Windows.Forms.ComboBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnInsertar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dtgPaises = new System.Windows.Forms.DataGridView();
            this.txtBuscarpais = new System.Windows.Forms.TextBox();
            this.cmbPais2 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtnoclubs = new System.Windows.Forms.TextBox();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.gpbPaises.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPaises)).BeginInit();
            this.SuspendLayout();
            // 
            // btnregresar
            // 
            this.btnregresar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnregresar.BackgroundImage")));
            this.btnregresar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnregresar.Location = new System.Drawing.Point(27, 31);
            this.btnregresar.Name = "btnregresar";
            this.btnregresar.Size = new System.Drawing.Size(60, 51);
            this.btnregresar.TabIndex = 41;
            this.btnregresar.UseVisualStyleBackColor = true;
            // 
            // gpbPaises
            // 
            this.gpbPaises.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.gpbPaises.Controls.Add(this.cmbPais2);
            this.gpbPaises.Controls.Add(this.cmbPAIS);
            this.gpbPaises.Controls.Add(this.btnCancelar);
            this.gpbPaises.Controls.Add(this.label3);
            this.gpbPaises.Controls.Add(this.label4);
            this.gpbPaises.Controls.Add(this.label2);
            this.gpbPaises.Controls.Add(this.txtnoclubs);
            this.gpbPaises.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbPaises.Location = new System.Drawing.Point(10, 345);
            this.gpbPaises.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gpbPaises.Name = "gpbPaises";
            this.gpbPaises.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gpbPaises.Size = new System.Drawing.Size(933, 192);
            this.gpbPaises.TabIndex = 39;
            this.gpbPaises.TabStop = false;
            this.gpbPaises.Text = "Paises";
            // 
            // cmbPAIS
            // 
            this.cmbPAIS.FormattingEnabled = true;
            this.cmbPAIS.Location = new System.Drawing.Point(234, 55);
            this.cmbPAIS.Name = "cmbPAIS";
            this.cmbPAIS.Size = new System.Drawing.Size(169, 37);
            this.cmbPAIS.TabIndex = 10;
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnCancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(733, 90);
            this.btnCancelar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(180, 62);
            this.btnCancelar.TabIndex = 7;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 123);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 29);
            this.label3.TabIndex = 9;
            this.label3.Text = "Representa a";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 55);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 29);
            this.label2.TabIndex = 5;
            this.label2.Text = "Pais";
            // 
            // btnEliminar
            // 
            this.btnEliminar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEliminar.BackColor = System.Drawing.Color.Red;
            this.btnEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminar.Image")));
            this.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminar.Location = new System.Drawing.Point(758, 162);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(165, 62);
            this.btnEliminar.TabIndex = 38;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEliminar.UseVisualStyleBackColor = false;
            // 
            // btnInsertar
            // 
            this.btnInsertar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnInsertar.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnInsertar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsertar.Image = ((System.Drawing.Image)(resources.GetObject("btnInsertar.Image")));
            this.btnInsertar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInsertar.Location = new System.Drawing.Point(758, 90);
            this.btnInsertar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnInsertar.Name = "btnInsertar";
            this.btnInsertar.Size = new System.Drawing.Size(165, 62);
            this.btnInsertar.TabIndex = 37;
            this.btnInsertar.Text = "Insertar";
            this.btnInsertar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInsertar.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(133, 39);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 29);
            this.label1.TabIndex = 36;
            this.label1.Text = "Buscar Pais";
            // 
            // dtgPaises
            // 
            this.dtgPaises.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dtgPaises.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgPaises.Location = new System.Drawing.Point(10, 90);
            this.dtgPaises.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dtgPaises.Name = "dtgPaises";
            this.dtgPaises.Size = new System.Drawing.Size(728, 223);
            this.dtgPaises.TabIndex = 34;
            // 
            // txtBuscarpais
            // 
            this.txtBuscarpais.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtBuscarpais.Location = new System.Drawing.Point(319, 43);
            this.txtBuscarpais.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtBuscarpais.Name = "txtBuscarpais";
            this.txtBuscarpais.Size = new System.Drawing.Size(419, 26);
            this.txtBuscarpais.TabIndex = 33;
            // 
            // cmbPais2
            // 
            this.cmbPais2.FormattingEnabled = true;
            this.cmbPais2.Location = new System.Drawing.Point(234, 120);
            this.cmbPais2.Name = "cmbPais2";
            this.cmbPais2.Size = new System.Drawing.Size(169, 37);
            this.cmbPais2.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(422, 90);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 29);
            this.label4.TabIndex = 5;
            this.label4.Text = "No. Clubs";
            // 
            // txtnoclubs
            // 
            this.txtnoclubs.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtnoclubs.Location = new System.Drawing.Point(558, 90);
            this.txtnoclubs.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtnoclubs.Name = "txtnoclubs";
            this.txtnoclubs.Size = new System.Drawing.Size(137, 33);
            this.txtnoclubs.TabIndex = 33;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnGuardar.BackColor = System.Drawing.Color.Lime;
            this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.Image")));
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardar.Location = new System.Drawing.Point(758, 234);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(165, 62);
            this.btnGuardar.TabIndex = 42;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardar.UseVisualStyleBackColor = false;
            // 
            // frmPaises
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ClientSize = new System.Drawing.Size(974, 568);
            this.ControlBox = false;
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnregresar);
            this.Controls.Add(this.gpbPaises);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnInsertar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtgPaises);
            this.Controls.Add(this.txtBuscarpais);
            this.Name = "frmPaises";
            this.gpbPaises.ResumeLayout(false);
            this.gpbPaises.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgPaises)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnregresar;
        private System.Windows.Forms.GroupBox gpbPaises;
        private System.Windows.Forms.ComboBox cmbPAIS;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnInsertar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dtgPaises;
        private System.Windows.Forms.TextBox txtBuscarpais;
        private System.Windows.Forms.ComboBox cmbPais2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtnoclubs;
        private System.Windows.Forms.Button btnGuardar;
    }
}