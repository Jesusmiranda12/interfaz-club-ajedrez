﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clubajedrez
{
    public partial class Form1 : Form
    {
        private Timer ti;
        private Form formularioactivo = null;
        public Form1()
        {
            ti = new Timer();
            ti.Tick += new EventHandler(eventoTimer);
            InitializeComponent();
            ti.Enabled = true;
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
        }
        private void eventoTimer(object ob, EventArgs evt)
        {
            DateTime hoy = DateTime.Now;
            lblReloj.Text = hoy.ToString("hh:mm:ss tt");
        }
        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
           
        }

        private void button10_Click(object sender, EventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MenuVertical.Width = 63;
            MenuContenido.Width = 808;
            //MenuVertical.Visible = true;
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            if (MenuVertical.Width == 200)
            {
                MenuVertical.Width = 63;
               
              
            }
            else
            {
                MenuVertical.Width = 200;
               
                
            }
        }

        private void btnMaximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            btnRestaurar.Visible = true;
            btnMaximizar.Visible = false;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            
        }

        private void btnCerrar_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnParticipantes_Click(object sender, EventArgs e)
        {
            abrir( new FrmParticipantes());
        }
        private void abrir(Form abrir)
        {
            if (formularioactivo != null)
            {
                formularioactivo.Close();
            }
            formularioactivo = abrir;
            abrir.TopLevel = false;
            abrir.FormBorderStyle = FormBorderStyle.None;
            abrir.Dock = DockStyle.Fill;
            MenuContenido.Controls.Add(abrir);
            MenuContenido.Tag = abrir;
            abrir.BringToFront();
            abrir.Show();
        }
  
        private void MenuContenido_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnMaximizar_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            btnRestaurar.Visible = true;
            btnMaximizar.Visible = false;
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnSalas_Click(object sender, EventArgs e)
        {
            abrir(new frmSalas());
        }

        private void btnRestaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            btnRestaurar.Visible = false;
            btnMaximizar.Visible = true;
        }

        private void btnHoteles_Click(object sender, EventArgs e)
        {
            abrir(new frmHoteles());
        }

        private void btnPartidas_Click(object sender, EventArgs e)
        {
            abrir(new frmPartidas());
        }

        private void btnHospedajes_Click(object sender, EventArgs e)
        {
            abrir(new FrmHospedajes());
        }

        private void btnJugadores_Click(object sender, EventArgs e)
        {
            abrir(new frmJugadores());
        }

        private void btnArbitros_Click(object sender, EventArgs e)
        {
            abrir(new frmArbitros());
        }

        private void btnPaises_Click(object sender, EventArgs e)
        {
            abrir(new frmPaises());
        }

        private void btnMovimientos_Click(object sender, EventArgs e)
        {
            abrir(new FrmMovimientos());
        }
    }
}
