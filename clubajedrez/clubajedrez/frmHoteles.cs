﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using Logicadenegocio;

namespace clubajedrez
{
    public partial class frmHoteles : Form
    {
        private HotelesManejador _hotelesManejador;
        public frmHoteles()
        {
            InitializeComponent();
            _hotelesManejador = new HotelesManejador();
        }
        private void limpiar()
        {
            lblId.Text = 0.ToString();          
            txtNombre.Text = "";
            txtDireccion.Text = "";
            txtTelefono.Text = "";
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("estas seguro que deceas eliminar este registro", "eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)

            {
                try
                {
                    eliminar();
                    buscar("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }
        private void eliminar()
        {
            var id = dtgHoteles.CurrentRow.Cells["Idhotel"].Value;
            _hotelesManejador.Eliminar(Convert.ToInt32(id));

        }
        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscar(txtBuscar.Text);
        }

        private void frmHoteles_Load(object sender, EventArgs e)
        {
            gpbHoteles.Visible = false;
            buscar("");
        }

        private void dtgHoteles_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            gpbHoteles.Visible = true;
            txtNombre.Enabled = true;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Guardar();
            buscar("");
            limpiar();
            gpbHoteles.Visible = false;
        }
        private void Guardar()
        {
            _hotelesManejador.Guardar(new Hoteles
            {
                IdHotel = Convert.ToInt32(lblId.Text),
                Nombreh = txtNombre.Text,
                Direccion = txtDireccion.Text,
                Telefono = txtTelefono.Text
            });

        }
        private void buscar(string filtro)
        {
            dtgHoteles.DataSource = _hotelesManejador.GetHoteles(filtro);
        }
        private void modificar()
        {
            lblId.Text = dtgHoteles.CurrentRow.Cells["Idhotel"].Value.ToString();
            txtNombre.Text = dtgHoteles.CurrentRow.Cells["Nombreh"].Value.ToString();
            txtDireccion.Text = dtgHoteles.CurrentRow.Cells["Direccion"].Value.ToString();
            txtTelefono.Text = dtgHoteles.CurrentRow.Cells["Telefono"].Value.ToString();
        }
        private void dtgHoteles_DoubleClick(object sender, EventArgs e)
        {
            gpbHoteles.Visible = true;
            try
            {
                modificar();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiar();
            gpbHoteles.Visible = false;
        }
    }
}
