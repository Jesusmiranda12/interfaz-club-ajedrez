﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;
using Logicadenegocio;
using System.IO;
namespace clubajedrez
{
    
    public partial class FrmParticipantes : Form
    {
        private string ruta;
        private OpenFileDialog _archivo;
            private Image Nothing = null;
        private  ParticipantesManejador _participanteManejador;
        private PaisManejador _paisManejador;
        public FrmParticipantes()
        {
            InitializeComponent();
            _participanteManejador = new ParticipantesManejador();
            _archivo = new OpenFileDialog();
            _paisManejador = new PaisManejador();
        }

        private void Participantes_Load(object sender, EventArgs e)
        {
            txtlogo.Visible = false;
            ruta = Application.StartupPath + "\\ PARTICIPANTES \\";
            paisCombo(" ");
            buscar(" ");
            buscar("");
      
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Guardar();
            guardarimagen();
            buscar(" ");
            buscar("");
            limpiar();
        }
        private void Guardar()
        {
            _participanteManejador.Guardar(new Participantes
            {
                IdParticipante = Convert.ToInt32(LblID.Text),
                NumeroAsociado = Convert.ToInt32(txtNumeroAsociado.Text),
                Nombre = txtNombre.Text,
                ApellidoP = txtApellidoPaterno.Text,
                ApellidoM = txtApellidoMaterno.Text,
                Direccion = txtDireccion.Text,
                Telefono = txttelefono.Text,
                Pais = cmbPais.SelectedValue.ToString(),
                Foto=txtlogo.Text
            });
            
        }
        private void eliminar()
        {
            var id = dtgParticipantes.CurrentRow.Cells["idparticipante"].Value;
            _participanteManejador.Eliminar(Convert.ToInt32( id));

        }
        private void modificar()
        {
            LblID.Text = dtgParticipantes.CurrentRow.Cells["IdParticipante"].Value.ToString();
            txtNumeroAsociado.Text=dtgParticipantes.CurrentRow.Cells["NumeroAsociado"].Value.ToString();
            txtNombre.Text= dtgParticipantes.CurrentRow.Cells["Nombre"].Value.ToString();
            txtApellidoPaterno.Text= dtgParticipantes.CurrentRow.Cells["ApellidoP"].Value.ToString();
            txtApellidoMaterno.Text= dtgParticipantes.CurrentRow.Cells["ApellidoM"].Value.ToString();
            txtDireccion.Text= dtgParticipantes.CurrentRow.Cells["Direccion"].Value.ToString();
            txttelefono.Text= dtgParticipantes.CurrentRow.Cells["Telefono"].Value.ToString();
            cmbPais.Text= dtgParticipantes.CurrentRow.Cells["Pais"].Value.ToString();
            txtlogo.Text = dtgParticipantes.CurrentRow.Cells["foto"].Value.ToString();
            string direccion = ruta + txtlogo.Text;
            pblogo.ImageLocation = direccion;
            pblogo.SizeMode = PictureBoxSizeMode.StretchImage;
        }
        private void buscar(string filtro)
        {
            dtgParticipantes.DataSource = _participanteManejador.GetParticipantes(filtro);
        }
        private void paisCombo(string filtro)
        {
            cmbPais.DataSource = _paisManejador.GetPais(filtro);
            cmbPais.ValueMember = "IdPaid";
            cmbPais.DisplayMember = "NombrePais";
        }

        private void cmbPais_Click(object sender, EventArgs e)
        {
            paisCombo(" ");
            paisCombo("");
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            buscar(txtBuscar.Text);
        }

        private void dtgParticipantes_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                modificar();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            
        }
        private void limpiar()
        {
            LblID.Text = 0.ToString();
            txtNumeroAsociado.Text = "";
            txtNombre.Text = "";
            txtApellidoPaterno.Text = "";
            txtApellidoMaterno.Text = "";
            txtDireccion.Text = "";
            txttelefono.Text = "";
            cmbPais.Text = "";
            pblogo.Image = Nothing;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpiar();
        }
      
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("estas seguro que deceas eliminar este registro", "eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)

            {
                try
                {
                    eliminar();
                    buscar("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }

        private void btnImagen_Click(object sender, EventArgs e)
        {
            _archivo.Filter =" Imagen tipo(*.jpg) | *.jpg | (*.png) | *.png";
            _archivo.Title = "cargar imagen";
            _archivo.ShowDialog();
            string direccion = _archivo.FileName;
            pblogo.ImageLocation = direccion;
            pblogo.SizeMode = PictureBoxSizeMode.StretchImage;
            if (_archivo.FileName != "")
            {
                var archivo = new FileInfo(_archivo.FileName);
                txtlogo.Text = archivo.Name;
            }

        }

        private void txtlogo_TextChanged(object sender, EventArgs e)
        {

        }
        private void guardarimagen()
        {
           
            
            try
            {
                if (_archivo.FileName != null)
                {
                    if (_archivo.FileName != "")
                    {
                        var document = new FileInfo(_archivo.FileName);
                        if (Directory.Exists(ruta))
                        {

                            //codigo para aggregar el archivo
                            if (Directory.Exists(ruta) == false)
                            {


                                var obtenerArchivps = Directory.GetFiles(ruta, _archivo.Filter);


                                FileInfo archivoanterior;
                                if (obtenerArchivps.Length != 0)
                                {
                                    //codigo para remplazar imagen
                                    archivoanterior = new FileInfo(obtenerArchivps[0]);

                                    if (archivoanterior.Exists)
                                    {
                                        archivoanterior.Delete();
                                        document.CopyTo(ruta + document.Name);

                                    }
                                }
                            }
                            else
                            {
                                document.CopyTo(ruta + document.Name);
                            }
                        }
                        else
                        {
                            Directory.CreateDirectory(ruta);
                            document.CopyTo(ruta + document.Name);
                        }
                    }
                }
            }
            catch (Exception )
            {
                MessageBox.Show("La imagen ya ha sido guardada");
            }
        }

        private void btncancelarFoto_Click(object sender, EventArgs e)
        {
            var nombre = txtlogo.Text;
            var ruta1 = ruta + nombre;
            if (MessageBox.Show("¿estas seguro que deceas eliminar la imagen? los cambios no se podran deshacer", "eliminar", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    txtlogo.Text = "";
                    pblogo.Image = Nothing;
                    MessageBox.Show(ruta1);
                    if(Directory.Exists(ruta1))
                    {
                        File.Delete(ruta1);
                    }
                  
                   
                    
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {

        }
    }
}
