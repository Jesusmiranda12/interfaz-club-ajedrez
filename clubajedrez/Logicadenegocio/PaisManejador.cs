﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoaDatos;

namespace Logicadenegocio
{
    public class PaisManejador
    {
        PaisAccesoDatos _paisManejador = new PaisAccesoDatos();
        public List<Pais> GetPais(string filtro)
        {
            var listPais = _paisManejador.GetPaises(filtro);
            return listPais;
        }

    }
}
