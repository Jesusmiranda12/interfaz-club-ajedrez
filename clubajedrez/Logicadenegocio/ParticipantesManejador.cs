﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;
using AccesoaDatos;
namespace Logicadenegocio
{
   public class ParticipantesManejador
    {
        ParticipantesAccesoDatos _participanteManejador = new ParticipantesAccesoDatos();
        public void Guardar(Participantes participante)
        {
            _participanteManejador.Guardar(participante);
        }
        public void Eliminar(int ID)
        {
            _participanteManejador.Eliminar(ID);
        }
        public List<Participantes> GetParticipantes(string filtro)
        {
            var listParticipantes = _participanteManejador.GetParticipantes(filtro);
            return listParticipantes;
        }

    }
}
